<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $table = 'feeds';

    protected $guarded = ['id'];

    public function category()
    {
        return $this->hasMany(\App\FeedCategory::class, 'feed_id', 'id');
    }
}
