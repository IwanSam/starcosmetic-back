<?php

namespace App\Http\Controllers\Backoffice;

use App\Category;
use App\Feed;
use App\FeedCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeds = Feed::with('category')->get();
        $i = 0;
        foreach ($feeds as $ff) {
            $feeds[$i]->feed_category = $ff->category->implode('category',', ');
            $i++;
        }

        //dd($feeds);
        return view('backoffice.feed.index', compact('feeds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', TRUE)->get();
        return view('backoffice.feed.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $request->validate([
            'image_upload'  => 'required|image|mimes:jpg,png,jpeg,gif,svg',
            'title'          => 'required|string',
            'description'   => 'required|string',
        ]);

        if ($request->hasFile('image_upload')) {
            $image = $request->file('image_upload');
            $folder = public_path() . '/uploads/feeds';

            if (!File::exists($folder)) {
                File::makeDirectory($folder, 0777, true);
            }

            Image::make($image)->save( $folder .'/'. $image->getClientOriginalName());

            $request->merge([
                'image' => "/uploads/feeds/" . $image->getClientOriginalName(),
            ]);
        }

        $request->merge([
            "slug"          => Str::slug($request->title)
        ]);
        //dd($request);

        $feedCategory = [];
        foreach ($request->category as $value){
            $feedCategory[] = [
                'category_id'   => $value,
                'category'      => Category::where('id', $value)->first()->name,
            ];
        }

        $feed = Feed::create($request->except('_token', '_method', 'image_upload', 'category'));
        $feed->category()->createMany($feedCategory);

        return redirect()->route('feeds.index')->with('success', 'Feed has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feed = Feed::where('id', $id)->with('category')->first();
        $categories = Category::where('status', TRUE)->get();

        return view('backoffice.feed.edit', compact('feed', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'          => 'required|string',
            'description'   => 'required|string',
        ]);

        $getEditFeed = Feed::where('id', $id)->first();

        $request->merge([
            "slug"          => Str::slug($request->title)
        ]);
        //dd($request);

        $feedCategory = [];
        foreach ($request->category as $value){
            $feedCategory[] = [
                'category_id'   => $value,
                'category'      => Category::where('id', $value)->first()->name,
            ];
        }

        Feed::where('id', $id)->update($request->except('_token', '_method', 'category'));
        FeedCategory::where('feed_id', $id)->delete();

        $getEditFeed->category()->createMany($feedCategory);

        return redirect()->route('feeds.index')->with('success', "Feed \"$getEditFeed->title\" has been updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldPage = Feed::where('id',$id)->firstOrFail();
        if($oldPage->image) File::delete($oldPage->image);

        FeedCategory::whereIn('feed_id', [$oldPage->id])->delete();

        $oldPage->delete();
        return redirect()->route('feeds.index')->with('success', 'Feed has been deleted.');
    }

    public function updateImage(Request $request)
    {
        //dd($request);
        $oldBanner = Feed::where('id', $request->id)->firstOrFail();

        $request->validate([
            'image_upload' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
        ]);

        if ($request->hasFile('image_upload')) {
            $image = $request->file('image_upload');
            $namaFile = $image->getClientOriginalName();
            $folder = public_path() . '/uploads/feeds';

            $image->move($folder, $namaFile);

            File::delete(public_path() . $oldBanner->image);

            $request->merge([
                'image' => "/uploads/feeds/" . $image->getClientOriginalName(),
            ]);
        }

        $oldBanner->update($request->except('_token', '_method', 'id', 'image_upload'));

        return redirect()->route('feeds.index')->with('success', 'The feed has been updated.');
    }
}
