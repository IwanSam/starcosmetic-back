<?php

namespace App\Http\Controllers\Backoffice;

use App\Feed;
use App\Category;
use App\Http\Controllers\Controller;
use App\Sosmed;
use App\User;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function activeInactive(Request $request)
    {
        $string = "\App\\".ucfirst(substr($request->model, 0, -1));

        $model = new $string;

        $model = $model->where('id', $request->id)->first();

        if($model){

            $model->update([
                'status' => !$model->status
            ]);

            return redirect()->back()->with('success', 'Update status successful');
        }

        return redirect()->back()->with('errors', 'data tidak ada');

    }
}
