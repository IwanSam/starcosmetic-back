<?php

namespace App\Http\Controllers\Backoffice;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::join('brands', 'brands.id', '=', 'products.brand_id')
                            ->select(DB::raw('products.*, brands.name as brand'))
                            ->get();

        //dd($products);

        return view('backoffice.master.product.item.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::get();

        return view('backoffice.master.product.item.add', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image_upload' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
            'name'  => 'required|string',
            'brand_id' => 'required|string|min:1',
            'link' => 'required|string',
        ]);

        if ($request->hasFile('image_upload')) {
            $image = $request->file('image_upload');
            $folder = public_path() . '/uploads/products';

            if (!File::exists($folder)) {
                File::makeDirectory($folder, 0777, true);
            }

            Image::make($image)->save( $folder .'/'. $image->getClientOriginalName());

            $request->merge([
                'image' => "/uploads/products/" . $image->getClientOriginalName(),
            ]);
        }

        //dd($request);
        Product::create($request->except('_token', '_method', 'image_upload'));

        return redirect()->route('products.index')->with('success', 'Product item has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = Brand::get();
        $product = Product::where('id', $id)->first();

        return view('backoffice.master.product.item.edit', compact('brands', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required|string',
            'brand_id' => 'required|string|min:1',
            'link' => 'required|string',
        ]);

        Product::where('id', $id)->update($request->except('_token', '_method'));

        return redirect()->route('products.index')->with('success', 'Product item has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldPage = Product::where('id',$id)->firstOrFail();
        if($oldPage->image) File::delete($oldPage->image);

        $oldPage->delete();
        return redirect()->route('feeds.index')->with('success', 'Product has been deleted.');
    }

    public function updateImage(Request $request)
    {
        //dd($request);
        $oldProduct = Product::where('id', $request->id)->firstOrFail();

        $request->validate([
            'image_upload' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
        ]);

        if ($request->hasFile('image_upload')) {
            $image = $request->file('image_upload');
            $namaFile = $image->getClientOriginalName();
            $folder = public_path() . '/uploads/products';

            $image->move($folder, $namaFile);

            File::delete(public_path() . $oldProduct->image);

            $request->merge([
                'image' => "/uploads/products/" . $image->getClientOriginalName(),
            ]);
        }

        $oldProduct->update($request->except('_token', '_method', 'id', 'image_upload'));

        return redirect()->route('products.index')->with('success', 'The product has been updated.');
    }
}
