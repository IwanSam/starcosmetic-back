<?php

namespace App\Http\Controllers\Backoffice;

use Intervention\Image\Facades\Image;
use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::get();

        return view('backoffice.master.banner.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.master.banner.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image_upload' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
            'name'  => 'required|string',
        ]);

        if ($request->hasFile('image_upload')) {
            $image = $request->file('image_upload');
            $folder = public_path() . '/uploads/banners';

            if (!File::exists($folder)) {
                File::makeDirectory($folder, 0777, true);
            }

            Image::make($image)->save( $folder .'/'. $image->getClientOriginalName());

            $request->merge([
                'image' => "/uploads/banners/" . $image->getClientOriginalName(),
            ]);
        }

        //dd($request);
        Banner::create($request->except('_token', '_method', 'image_upload'));

        return redirect()->route('banners.index')->with('success', 'Banner has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::where('id', $id)->first();

        return view('backoffice.master.banner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldBanner = Banner::where('id', $id)->firstOrFail();
        $request->validate([
            'name'  => 'required|string',
        ]);

        $oldBanner->update($request->except('_token', '_method'));

        return redirect()->route('banners.index')->with('success', 'The banner has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        //
    }

    public function updateImage(Request $request)
    {
        //dd($request);
        $oldBanner = Banner::where('id', $request->id)->firstOrFail();

        $request->validate([
            'image_upload' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
        ]);

        if ($request->hasFile('image_upload')) {
            $image = $request->file('image_upload');
            $namaFile = $image->getClientOriginalName();
            $folder = public_path() . '/uploads/banners';

            $image->move($folder, $namaFile);

            File::delete(public_path() . $oldBanner->image);

            $request->merge([
                'image' => "/uploads/banners/" . $image->getClientOriginalName(),
            ]);
        }

        $oldBanner->update($request->except('_token', '_method', 'id', 'image_upload'));

        return redirect()->route('banners.index')->with('success', 'The banner has been updated.');
    }
}
