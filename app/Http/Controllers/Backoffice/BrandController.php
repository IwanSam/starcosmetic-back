<?php

namespace App\Http\Controllers\Backoffice;

use App\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::get();

        return view('backoffice.master.product.brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.master.product.brand.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image_upload'  => 'required|image|mimes:jpg,png,jpeg,gif,svg',
            'name'          => 'required|string',
            'link'          => 'required|string',
        ]);

        if($request->link==='-'){
            $request->merge([
                'link' => '#'
            ]);
        }

        if ($request->hasFile('image_upload')) {
            $image = $request->file('image_upload');
            $folder = public_path() . '/uploads/brands';

            if (!File::exists($folder)) {
                File::makeDirectory($folder, 0777, true);
            }

            Image::make($image)->save( $folder .'/'. $image->getClientOriginalName());

            $request->merge([
                'image' => "/uploads/brands/" . $image->getClientOriginalName(),
            ]);
        }

        //dd($request);
        Brand::create($request->except('_token', '_method', 'image_upload'));

        return redirect()->route('brands.index')->with('success', 'Brand has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::where('id', $id)->first();

        return view('backoffice.master.product.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldBanner = Brand::where('id', $id)->firstOrFail();
        $request->validate([
            'name'  => 'required|string',
            'link'  => 'required|string',
        ]);

        $oldBanner->update($request->except('_token', '_method'));

        return redirect()->route('brands.index')->with('success', 'The brand has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        //
    }

    public function updateImage(Request $request)
    {
        //dd($request);
        $oldBanner = Brand::where('id', $request->id)->firstOrFail();

        $request->validate([
            'image_upload' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
        ]);

        if ($request->hasFile('image_upload')) {
            $image = $request->file('image_upload');
            $namaFile = $image->getClientOriginalName();
            $folder = public_path() . '/uploads/brands';

            $image->move($folder, $namaFile);

            File::delete(public_path() . $oldBanner->image);

            $request->merge([
                'image' => "/uploads/brands/" . $image->getClientOriginalName(),
            ]);
        }

        $oldBanner->update($request->except('_token', '_method', 'id', 'image_upload'));

        return redirect()->route('brands.index')->with('success', 'The image brand has been updated.');
    }
}
