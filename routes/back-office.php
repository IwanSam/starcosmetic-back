<?php

use App\Http\Controllers\Backoffice\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function(){

    Route::view('/dashboard', 'backoffice.dashboard')->name('dashboard');

    Route::get('active-inactive','Backoffice\MasterController@activeInactive')->name('active-inactive');

    Route::resource('users','Backoffice\UserController')->except(['create', 'store','delete']);

    Route::resource('sosmeds','Backoffice\SosmedController')->except(['delete', 'edit']);
    Route::post('sosmeds-link', 'Backoffice\SosmedController@linkUpdate')->name('sosmeds.link');

    Route::resource('feeds','Backoffice\FeedController');
    Route::post('feeds-image', 'Backoffice\FeedController@updateImage')->name('feeds.image');

    Route::resource('categories','Backoffice\CategoryController')->except(['delete']);

    Route::resource('banners','Backoffice\BannerController')->except(['delete']);
    Route::post('banners-image', 'Backoffice\BannerController@updateImage')->name('banners.image');

    Route::resource('brands','Backoffice\BrandController')->except(['delete']);
    Route::post('brands-image', 'Backoffice\BrandController@updateImage')->name('brands.image');

    Route::resource('products','Backoffice\ProductController');
    Route::post('products-image', 'Backoffice\ProductController@updateImage')->name('products.image');

});
