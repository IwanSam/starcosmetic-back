@extends('layouts.backoffice')

@section('title', 'Dashboard')
@section('header-title', 'Dashboard')
@section('menu_dashboard', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard </li>
@endsection

@section('content')
<div class="row">
    <!-- metric -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h5 class="text-muted">{{ Auth::user()->name }}</h5>
                <div class="metric-value d-inline-block">
                    <h1 class="mb-1 text-primary">Welcome Back </h1>
                </div>
            </div>
            <div id="sparkline-1"></div>
        </div>
    </div>
</div>
@endsection
