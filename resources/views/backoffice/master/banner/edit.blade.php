@extends('layouts.backoffice')

@section('title', 'Banner')
@section('header-title', 'Edit Banner')
@section('menu_master_banner', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)" class="breadcrumb-link">Master</a></li>
<li class="breadcrumb-item"><a href="{{ route('banners.index') }}" class="breadcrumb-link">Banner</a></li>
<li class="breadcrumb-item active" aria-current="page">Edit </li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- basic form  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('banners.update', $banner->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label class="col-form-label">Banner :</label>
                    </div>
                    <div class="img-container mb-3">
                        <img style="border-radius: .25rem;" id="image-preview" src="{{ (!in_array($banner->image, [null, ""]) ) ? asset($banner->image) : asset('assets/images/dummy-image.png') }}" width="100%" height="400" alt="Picture">
                    </div>

                    {{-- <div class="custom-file mb-3">
                        <input type="file" class="custom-file-input @error('image_upload') is-invalid @enderror" id="image" name="image_upload">
                        <label class="custom-file-label" for="image">Add Image <span class="text-danger">*</span></label>
                        @error('image_upload')
                            <div class="invalid-feedback">Please choose a banner image.</div>
                        @enderror
                    </div> --}}
                    <div class="form-group">
                        <label for="inputText3" class="col-form-label">Name <span class="text-danger">*</span></label>
                        <input id="inputText3" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $banner->name }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>Please provide a valid name.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description </label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="short_desc" placeholder="Place some text here">{{ $banner->short_desc }}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                        </div>
                        <div class="col-sm-6 pl-0">
                            <p class="text-right">
                                <button type="submit" class="btn btn-space btn-primary">Update</button>
                                <a href="{{ route('banners.index') }}" class="btn btn-space btn-secondary">Cancel</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end basic form  -->
<!-- ============================================================== -->
@endsection

@push('additional-scripts')
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#image').change(function(){
            let reader = new FileReader();
            reader.onload = (e) => {
                $('#image-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        });
    });
</script>
@endpush
