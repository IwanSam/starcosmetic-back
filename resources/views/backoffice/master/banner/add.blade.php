@extends('layouts.backoffice')

@section('title', 'Banner')
@section('header-title', 'Add Banner')
@section('menu_master_banner', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)" class="breadcrumb-link">Master</a></li>
<li class="breadcrumb-item"><a href="{{ route('banners.index') }}" class="breadcrumb-link">Banner</a></li>
<li class="breadcrumb-item active" aria-current="page">Add </li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- basic form  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('banners.store') }}" method="POST" enctype="multipart/form-data">

                    <div class="form-group">
                        <label class="col-form-label">Preview :</label>
                        <div class="img-container mb-3" @error('image_upload') style="border: 1px solid #dc3545 !important; padding: .25rem; border-radius: .25rem;" @enderror>
                            <center>
                                <img style="border-radius: .25rem; max-height: 400px;" id="image-preview" src="{{ asset('assets/images/dummy-image.png') }}" width="100%" height="100%" alt="Picture">
                            </center>
                        </div>
                    </div>

                    <div class="custom-file mb-3">
                        <input type="file" class="custom-file-input @error('image_upload') is-invalid @enderror" id="image" name="image_upload">
                        <label class="custom-file-label" for="image">Add Image <span class="text-danger">*</span></label>
                        @error('image_upload')
                            <div class="invalid-feedback">Please choose a banner image.</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputText3" class="col-form-label">Name <span class="text-danger">*</span></label>
                        <input id="inputText3" type="text" class="form-control @error('name') is-invalid @enderror" name="name">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>Please provide a valid name.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description </label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="short_desc"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">
                            @csrf
                        </div>
                        <div class="col-sm-6 pl-0">
                            <p class="text-right">
                                <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                <a href="{{ route('banners.create') }}" class="btn btn-space btn-secondary">Cancel</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end basic form  -->
<!-- ============================================================== -->
@endsection

@push('additional-scripts')
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#image').change(function(){
            let reader = new FileReader();
            reader.onload = (e) => {
                $('#image-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        });
    });
</script>
@endpush
