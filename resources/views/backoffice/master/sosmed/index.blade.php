@extends('layouts.backoffice')

@section('title', 'Sosmed')
@section('header-title', 'Sosmed')
@section('menu_master_sosmed', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)" class="breadcrumb-link">Master</a></li>
<li class="breadcrumb-item active" aria-current="page">Sosmed </li>
@endsection

@section('content')
<div class="row">
    <!-- ============================================================== -->
    <!-- basic table  -->
    <!-- ============================================================== -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered first" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Name</th>
                                <th class="text-center">Icon</th>
                                <th>Link</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sosmeds as $sosmed)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td>
                                    @if ($sosmed->link=='#')
                                        {{ $sosmed->name }}
                                    @elseif($sosmed->status)
                                        <a href="{{ $sosmed->link }}" target="_blank">{{ $sosmed->name }}</a>
                                    @else
                                        {{ $sosmed->name }}
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if (explode(' ', $sosmed->icon)[0]=='fab')
                                        <span class="fab {{ explode(' ', $sosmed->icon)[1] }}"></span>
                                    @else
                                        <img width="20" height="20" src="{{ $sosmed->icon }}"/>
                                    @endif
                                </td>
                                <td>{{ $sosmed->link }}</td>
                                <td class="text-center">
                                    <span class="badge {{ ($sosmed->status == true) ? 'bg-success' : 'bg-danger' }}">{{ ($sosmed->status == true) ? 'Active' : 'In Active' }}</span>
                                </td>

                                <td class="text-center">
                                    <a href="{{ route('active-inactive', [ 'model' => 'sosmeds', 'id' => $sosmed->id ]) }}"
                                        class="btn btn-xs {{ ($sosmed->status) ? 'btn-outline-danger' : 'btn-outline-success' }}"
                                        data-toggle="tooltip" data-placement="top"
                                        title="{{ $sosmed->type }} {{($sosmed->status) ? 'In Active?': 'Active?'}}">

                                        <i class="{{($sosmed->status) ? 'fas fa-pause': 'fas fa-play'}}"></i>
                                    </a>

                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#modal" class="btn btn-xs btn-outline-primary"  data-toggle="tooltip"  data-original-title="Edit" data-id="{{ $sosmed->id }}" data-link="{{ $sosmed->link }}" data-name="{{ $sosmed->name }}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end basic table  -->
    <!-- ============================================================== -->

    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('sosmeds.link') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                    @csrf

                    <input type="hidden" name="id" id="idSosmed" value="">

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </a>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="link" class="col-form-label">Link </label>
                            <input id="link" type="text" class="link form-control @error('link') is-invalid @enderror" name="link" value="">
                            @error('link')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Please provide a valid name.</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Close</a>
                        <button type="submit" id="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('additional-scripts')
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#modal').on('show.bs.modal', function(e){
            var name = $(e.relatedTarget).data('name');
            var link = $(e.relatedTarget).data('link');
            var id = $(e.relatedTarget).data('id');

            $(".link").attr('value', link);
            $("#exampleModalLabel").text('Change Link for ' + name);
            $('#idSosmed').attr('value', id);
            $('#submit').attr("disabled", "");
        });

        $('#modal').on('hide.bs.modal', function(e){
            $('.link').attr('value', '');
        });

        $('#link').change(function(){
            $('#submit').removeAttr('disabled');
        });

    });
</script>
@endpush
