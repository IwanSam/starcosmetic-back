@extends('layouts.backoffice')

@section('title', 'Product')
@section('header-title', 'Add Product')
@section('menu_master_product', 'active')
@section('menu_master_product_item', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)" class="breadcrumb-link">Master</a></li>
<li class="breadcrumb-item"><a href="{{ route('products.index') }}" class="breadcrumb-link">Product</a></li>
<li class="breadcrumb-item active" aria-current="page">Add </li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- basic form  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data" autocomplete="off">

                    <div class="form-group">
                        <label for="inputText3" class="col-form-label">Name <span class="text-danger">*</span></label>
                        <input id="inputText3" type="text" class="form-control @error('name') is-invalid @enderror" name="name">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>Please provide a valid name.</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="control-label">Brand <span class="text-danger">*</span></label>
                        <div class="">
                            <select class="form-control @error('brand_id') is-invalid @enderror" name="brand_id">
                                <option value="" selected disabled> - Select -</option>
                                @foreach ($brands as $brand)
                                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                            @error('brand_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Please select a category.</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputText4" class="col-form-label">Link <span class="text-danger">*</span></label>
                        <input id="inputText4" type="text" class="form-control @error('link') is-invalid @enderror" name="link">
                        @error('link')
                            <span class="invalid-feedback" role="alert">
                                <strong>Please provide a valid link.</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <div class="img-container" @error('image_upload') style="border: 1px solid #dc3545 !important; padding: .25rem; border-radius: .25rem;" @enderror>
                                    <img style="border-radius: .25rem; max-height: 100px; max-width: 100px;" id="image-preview" src="{{ asset('assets/images/image-feed.png') }}" width="100%" height="100%" alt="Picture">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-10 col-lg-10">
                                <div class="custom-file my-4">
                                    <input type="file" class="custom-file-input @error('image_upload') is-invalid @enderror" id="image" name="image_upload">
                                    <label class="custom-file-label" id="image_upload_label" for="image">Add Image <span class="text-danger">*</span></label>
                                    @error('image_upload')
                                        <div class="invalid-feedback">Please choose a banner image.</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">
                            @csrf
                        </div>
                        <div class="col-sm-6 pl-0">
                            <p class="text-right">
                                <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                <a href="{{ route('feeds.create') }}" class="btn btn-space btn-secondary">Cancel</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end basic form  -->
<!-- ============================================================== -->
@endsection

@push('additional-scripts')
<script type="text/javascript">
    $(document).ready(function (e) {

        $('#image').change(function(){
            let reader = new FileReader();
            reader.onload = (e) => {
                $('#image-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);

            $("#image_upload_label").text(this.files[0].name + " or Change?");
        });
    });
</script>
@endpush
