@extends('layouts.backoffice')

@section('title', 'Brand')
@section('header-title', 'Edit Brand')
@section('menu_master_product', 'active')
@section('menu_master_product_brand', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)" class="breadcrumb-link">Master</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)" class="breadcrumb-link">Product</a></li>
<li class="breadcrumb-item"><a href="{{ route('brands.index') }}" class="breadcrumb-link">Brand</a></li>
<li class="breadcrumb-item active" aria-current="page">Edit </li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- basic form  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('brands.update', $brand->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label class="col-form-label">Brand :</label>
                        <div class="img-container mb-3">
                            <center>
                                <img style="border-radius: .25rem; max-width: 600px; max-height: 400px;" id="image-preview" src="{{ (!in_array($brand->image, [null, ""]) ) ? asset($brand->image) : asset('assets/images/dummy-image.png') }}" width="100%" height="100%" alt="Picture">
                            </center>
                        </div>
                    </div>

                    {{-- <div class="custom-file mb-3">
                        <input type="file" class="custom-file-input @error('image_upload') is-invalid @enderror" id="image" name="image_upload">
                        <label class="custom-file-label" for="image">Add Image <span class="text-danger">*</span></label>
                        @error('image_upload')
                            <div class="invalid-feedback">Please choose a banner image.</div>
                        @enderror
                    </div> --}}
                    <div class="form-group">
                        <label for="inputText3" class="col-form-label">Name <span class="text-danger">*</span></label>
                        <input id="inputText3" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $brand->name }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>Please provide a valid name.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputText4" class="col-form-label">Link </label>
                        <input id="inputText4" type="text" class="form-control @error('link') is-invalid @enderror" name="link" value="{{ $brand->link }}">
                        @error('link')
                            <span class="invalid-feedback" role="alert">
                                <strong>Please provide a valid link.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                        </div>
                        <div class="col-sm-6 pl-0">
                            <p class="text-right">
                                <button type="submit" class="btn btn-space btn-primary">Update</button>
                                <a href="{{ route('brands.index') }}" class="btn btn-space btn-secondary">Cancel</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end basic form  -->
<!-- ============================================================== -->
@endsection

