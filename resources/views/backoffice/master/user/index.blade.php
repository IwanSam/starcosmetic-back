@extends('layouts.backoffice')

@section('title', 'User')
@section('header-title', 'User')
@section('menu_master_user', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)" class="breadcrumb-link">Master</a></li>
<li class="breadcrumb-item active" aria-current="page">User </li>
@endsection

@section('content')
<div class="row">
    <!-- ============================================================== -->
    <!-- basic table  -->
    <!-- ============================================================== -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered second" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th class="text-center">ID</th>
                                <th>User</th>
                                <th>Email</th>
                                <th class="text-center">Status</th>
                                <th>Role</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if($user->status)
                                    <span class="badge bg-success">Active</span>
                                    @else
                                    <span class="badge bg-danger">In Active</span>
                                    @endif
                                </td>
                                <td>{{ $user->role }}</td>
                                <td class="text-center">

                                    <a href="{{ route('active-inactive', [ 'model' => 'users', 'id' => $user->id ]) }}"
                                        class="btn btn-xs {{ ($user->status) ? 'btn-outline-danger' : 'btn-outline-success' }}"
                                        data-toggle="tooltip" data-placement="top"
                                        title="{{ $user->name }} {{($user->status) ? 'In Active?': 'Active?'}}">

                                        <i class="{{($user->status) ? 'fas fa-pause': 'fas fa-play'}}"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end basic table  -->
    <!-- ============================================================== -->
</div>
@endsection

