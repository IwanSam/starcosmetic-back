@extends('layouts.backoffice')

@section('title', 'Feed')
@section('header-title', 'Feed')
@section('menu_feed', 'active')
@section('menu_feed_feed', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Feed </li>
@endsection

@section('content')
<div class="row">
    <!-- ============================================================== -->
    <!-- basic table  -->
    <!-- ============================================================== -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-header">
                <a class="btn btn-sm btn-success float-right" href="{{ route('feeds.create') }}"><i class="fas fa-plus"></i> Add</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered second" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Name</th>
                                <th>Categories</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($feeds as $feed)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td>{{ $feed->title }}</td>
                                <td>{{ $feed->feed_category }}</td>
                                <td>
                                    @if(!in_array($feed->image, [null, ""]) )
                                        <img src="{{ asset($feed->image)}}" alt="{{ $feed->title }}" style="max-height: 120px; max-width: 120px">
                                    @else
                                        <img src="{{ asset('assets/images/no-image.png') }}" alt="No Image" style="max-height: 120px; max-width: 120px">
                                    @endif

                                    <a class="btn btn-xs btn-outline-success ml-3 {{ ($feed->status) ? '' : 'disabled' }}" href="javascript:void(0)" data-toggle="modal" data-target="#modal" data-image="{{ (!in_array($feed->image, [null, ""]) ) ? $feed->image : asset('assets/images/no-image.png') }}" data-title="{{ $feed->title }}" data-id="{{ $feed->id }}">
                                        <i class="fas fa-retweet"></i>
                                    </a>
                                </td>
                                <td>{{ $feed->description }}</td>
                                <td class="text-center">
                                    <span class="badge {{ ($feed->status == true) ? 'badge-success' : 'badge-danger' }}">{{ ($feed->status == true) ? 'Active' : 'In Active' }}</span>
                                </td>

                                <td class="text-center">
                                    <a href="{{ route('active-inactive', [ 'model' => 'feeds', 'id' => $feed->id ]) }}"
                                        class="mb-1 btn btn-xs {{ ($feed->status) ? 'btn-outline-danger' : 'btn-outline-success' }}"
                                        data-toggle="tooltip" data-placement="top"
                                        title="feed {{ $feed->name }} {{($feed->status) ? 'In Active?': 'Active?'}}">

                                        <i class="{{($feed->status) ? 'fas fa-pause': 'fas fa-play'}}"></i>
                                    </a>

                                    <a href="{{ route('feeds.edit', $feed->id) }}" class="btn btn-xs btn-outline-primary mb-1"  data-toggle="tooltip"
                                        data-original-title="Edit">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>

                                    <a href="{{ route('feeds.destroy', $feed->id) }}" class="btn btn-xs btn-outline-danger mb-1" onclick="
                                        var result = confirm('Are you sure you want to delete this record?');

                                        if(result){
                                            event.preventDefault();
                                            document.getElementById('delete-form-{{$feed->id}}').submit();
                                        }">
                                        <i class="fas fa-trash"></i>
                                    </a>

                                    <form method="POST" id="delete-form-{{$feed->id}}" action="{{ route('feeds.destroy', $feed->id) }}" style="display:hidden">
                                        @csrf
                                        @method("delete")
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end basic table  -->
    <!-- ============================================================== -->

    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('feeds.image') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    <input type="hidden" name="id" id="idImage" value="">

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </a>
                    </div>
                    <div class="modal-body">
                        <img class="imagePreview" src="{{ asset('assets/images/no-image.png') }}" alt="No Image" style="max-height: 300px; max-width: 300px;">

                        <div class="custom-file mb-3 mt-3">
                            <input type="file" class="imageUpload custom-file-input @error('image_upload') is-invalid @enderror" id="" name="image_upload">
                            <label class="custom-file-label" id="image_upload_label" for="image">Change </label>
                            @error('image_upload')
                                <div class="invalid-feedback">Please choose a banner image.</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Close</a>
                        <button type="submit" id="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('additional-scripts')
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#modal').on('show.bs.modal', function(e){
            var title = $(e.relatedTarget).data('title');
            var image = $(e.relatedTarget).data('image');
            var id = $(e.relatedTarget).data('id');

            $(".imagePreview").attr('src', image);
            $(".imagePreview").attr('alt', title);
            $("#exampleModalLabel").text('Change Image ' + title);
            $('#idImage').attr('value', id);
            $('#submit').attr("disabled", "");
        });

        $(".imageUpload").change(function(){
            let reader = new FileReader();
            reader.onload = (e) => {
                $(".imagePreview").attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);

            $("#image_upload_label").text(this.files[0].name + " or Change?");
            $('#submit').removeAttr('disabled');
        });
    });
</script>
@endpush
