@extends('layouts.backoffice')

@section('title', 'Feed')
@section('header-title', 'Edit Feed')
@section('menu_feed', 'active')
@section('menu_feed_feed', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('feeds.index') }}" class="breadcrumb-link">Feed</a></li>
<li class="breadcrumb-item active" aria-current="page">Edit </li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- basic form  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('feeds.update', $feed->id) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label for="inputText3" class="col-form-label">Title <span class="text-danger">*</span></label>
                        <input id="inputText3" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $feed->title }}">
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>Please provide a valid title.</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="control-label">Category <span class="text-danger">*</span></label>
                        <div class="">
                            <select class="js-example-basic-multiple @error('category') is-invalid @enderror" multiple="multiple" name="category[]">
                                @foreach ($categories as $item)
                                        <option value="{{ $item->id }}"
                                            @foreach ($feed->category as $cat)
                                                @if( $cat->category_id == $item->id )
                                                    selected
                                                @endif
                                            @endforeach
                                            >{{ $item->name }}</option>
                                    @endforeach
                            </select>
                            @error('category')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Please select a category.</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description <span class="text-danger">*</span></label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3" name="description">{{ $feed->description }}</textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>Please provide a valid description.</strong>
                            </span>
                        @enderror
                    </div>

                    {{-- <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <div class="img-container" @error('image_upload') style="border: 1px solid #dc3545 !important; padding: .25rem; border-radius: .25rem;" @enderror>
                                    <img style="border-radius: .25rem; max-height: 100px; max-width: 100px;" id="image-preview" src="{{ asset('assets/images/image-feed.png') }}" width="100%" height="100%" alt="Picture">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-10 col-lg-10">
                                <div class="custom-file my-4">
                                    <input type="file" class="custom-file-input @error('image_upload') is-invalid @enderror" id="image" name="image_upload">
                                    <label class="custom-file-label" id="image_upload_label" for="image">Add Image <span class="text-danger">*</span></label>
                                    @error('image_upload')
                                        <div class="invalid-feedback">Please choose a banner image.</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div> --}}

                    <div class="row">
                        <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                        </div>
                        <div class="col-sm-6 pl-0">
                            <p class="text-right">
                                <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                <a href="{{ route('feeds.create') }}" class="btn btn-space btn-secondary">Cancel</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end basic form  -->
<!-- ============================================================== -->
@endsection

@push('additional-scripts')
<script type="text/javascript">
    $(document).ready(function (e) {
        $('.js-example-basic-multiple').select2({ tags: true });
    });
</script>
@endpush

