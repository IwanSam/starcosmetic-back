@extends('layouts.backoffice')

@section('title', 'Category')
@section('header-title', 'Category')
@section('menu_feed', 'active')
@section('menu_feed_category', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)" class="breadcrumb-link">Feed</a></li>
<li class="breadcrumb-item active" aria-current="page">Category </li>
@endsection

@section('content')
<div class="row">
    <!-- ============================================================== -->
    <!-- basic table  -->
    <!-- ============================================================== -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-header">
                <a class="btn btn-sm btn-success float-right" href="{{ route('categories.create') }}"><i class="fas fa-plus"></i> Add</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered first" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Name</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td>{{ $category->name }}</td>
                                <td class="text-center">
                                    <span class="badge {{ ($category->status == true) ? 'bg-success' : 'bg-danger' }}">{{ ($category->status == true) ? 'Active' : 'In Active' }}</span>
                                </td>

                                <td class="text-center">
                                    <a href="{{ route('active-inactive', [ 'model' => 'categorys', 'id' => $category->id ]) }}"
                                        class="btn btn-xs {{ ($category->status) ? 'btn-outline-danger' : 'btn-outline-success' }}"
                                        data-toggle="tooltip" data-placement="top"
                                        title="{{ $category->type }} {{($category->status) ? 'In Active?': 'Active?'}}">

                                        <i class="{{($category->status) ? 'fas fa-pause': 'fas fa-play'}}"></i>
                                    </a>

                                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-xs btn-outline-primary"  data-toggle="tooltip"
                                        data-original-title="Edit">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end basic table  -->
    <!-- ============================================================== -->
</div>
@endsection

