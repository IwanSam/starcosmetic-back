@extends('layouts.backoffice')

@section('title', 'Category')
@section('header-title', 'Edit Category')
@section('menu_feed', 'active')
@section('menu_feed_category', 'active')

@section('br')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Home</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)" class="breadcrumb-link">Feed</a></li>
<li class="breadcrumb-item"><a href="{{ route('categories.index') }}" class="breadcrumb-link">Category</a></li>
<li class="breadcrumb-item active" aria-current="page">Edit </li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- basic form  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('categories.update', $category->id) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label for="inputText3" class="col-form-label">Name <span class="text-danger">*</span></label>
                        <input id="inputText3" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $category->name }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>Please provide a valid name.</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                        </div>
                        <div class="col-sm-6 pl-0">
                            <p class="text-right">
                                <button type="submit" class="btn btn-space btn-primary">Update</button>
                                <a href="{{ route('categories.index') }}" class="btn btn-space btn-secondary">Cancel</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end basic form  -->
<!-- ============================================================== -->
@endsection
