<?php

use Illuminate\Database\Seeder;

class AllTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #users
        App\User::insert([
            [
                "name"       => "superadmin",
                "email"      => "superadmin@starcosmeticsurabaya.com",
                "password"   => bcrypt("starcosmetic2021"),
                "image"      => "",
                "role"       => "super-admin",
                "created_at" => now(),
            ],
            [
                "name"       => "admin",
                "email"      => "admin@starcosmeticsurabaya.com",
                "password"   => bcrypt("starcosmetic2021"),
                "image"      => "",
                "role"       => "admin",
                "created_at" => now(),
            ]
        ]);

        #category
        App\Category::insert([
            [
                "name" => "Popular",
                "status" => true,
                "created_at" => now(),
            ],
            [
                "name" => "Feed",
                "status" => true,
                "created_at" => now(),
            ],
            [
                "name" => "Event",
                "status" => true,
                "created_at" => now(),
            ],
        ]);

        #sosmed
        App\Sosmed::insert([
            [
                "name"   => "whatsapp",
                "icon"   => "fab fa-whatsapp-square",
                "link"   => "#",
                "status" => true,
                "created_at" => now(),
            ],
            [
                "name"   => "facebook",
                "icon"   => "fab fa-facebook-square",
                "link"   => "#",
                "status" => true,
                "created_at" => now(),
            ],
            [
                "name"   => "instagram",
                "icon"   => "fab fa-instagram",
                "link"   => "#",
                "status" => true,
                "created_at" => now(),
            ],
            [
                "name"   => "shopee",
                "icon"   => "https://img.icons8.com/ios-filled/50/000000/shopee.png",
                "link"   => "https://shopee.co.id/starcosmeticsurabaya",
                "status" => true,
                "created_at" => now(),
            ],
        ]);
    }
}
